package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	
	// for length
	@Test
	public void testIsValidLoginRegular( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Jel012" ) );
	}
	
	@Test
	public void testIsValidLoginException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "j1276n" ) );
	}
	
	@Test
	public void testIsValidLoginBoundryIn( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "jex12" ) );
	}
	
	@Test
	public void testIsValidLoginBoundryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Jeel12345" ) );
	}
	
	// for alphanumeric
	
	@Test 
	public void LogInIsValidRegular() {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Je7l0123" ) );
		
	}
	
	@Test
	public void LogInIsValidException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "jeeln7" ) );
	}
	
	@Test
	public void LogInValidBoundaryIn( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "jeel8t" ) );
	}
	
	@Test
	public void LogInValidBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "jeel78dt" ) );
	}
	
	
}
